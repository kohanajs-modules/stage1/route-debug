const { KohanaJS } = require('kohanajs');

const printTable = (map, title = '') => {
  let text = '';
  text += `<details><summary>${title}</summary><table style="color:#AAA">`;
  map.forEach((v, k) => {
    text += `<tr><td>${k}</td><td>:</td><td>${v}</td></tr>`;
  });
  text += '</table></details>';
  return text;
};

const printList = (ary, title = '') => {
  let text = '';
  text += `<details><summary>${title}</summary><ul style="color:#AAA">`;
  ary.forEach(v => {
    text += `<li>${v}</li>`;
  });
  text += '</ul></details>';
  return text;
};

const execute = async (Controller, request) => {
  const { benchmarkReset, benchmark, getBenchmarkRecords } = KohanaJS.require('DevUtils');
  benchmarkReset();
  benchmark('start');

  // import controller
  const c = new Controller(request);
  benchmark('init Controller');

  const result = await c.execute();
  benchmark('exec Controller');

  const benchmarkOutput = JSON.stringify(getBenchmarkRecords().map(x => ({ label: x.label, ms: x.delta })));
  // eslint-disable-next-line no-console
  console.log(`${request.url} :::: ${benchmarkOutput}`);

  if (global.gc)global.gc();

  if (!result.headers['Content-Type']) {
    result.headers['Content-Type'] = 'text/html; charset=utf-8';
  }

  if (result.headers['Content-Type'] === 'application/javascript; charset=utf-8') {
    result.body = JSON.stringify(result.body);
    if (c.error) {
      // eslint-disable-next-line no-console
      console.log(c.error);
    }
    return result;
  }

  if (result.headers['Content-Type'] !== 'text/html; charset=utf-8') {
    // eslint-disable-next-line no-console
    if (c.error) { console.log(c.error); }
    return result;
  }

  let debugText = '';

  if (c.error) {
    debugText += `<pre style="color:#C00; display:inline;">${c.error.stack}</pre>`;
    debugText += '<hr style="border-color:#666"/>';
    // eslint-disable-next-line no-console
    console.log(c.error);
  }

  debugText += benchmarkOutput;
  debugText += '<hr style="border-color:#666"/>';

  debugText += printTable(KohanaJS.nodePackages, 'KohanaJS Node packages');
  debugText += '<hr style="border-color:#666"/>';
  debugText += printTable(KohanaJS.classPath, 'KohanaJS.required files');
  debugText += '<hr style="border-color:#666"/>';
  debugText += printList([...KohanaJS.viewPath.values()], 'Views');
  debugText += '<hr style="border-color:#666"/>';
  debugText += printTable(KohanaJS.configPath, 'Config files');
  debugText += '<hr style="border-color:#666"/>';

  const config = {
    classes: KohanaJS.config.classes,
    view: KohanaJS.config.view,
  };

  debugText += `<details><summary>Core Config Values</summary><pre style="color:#777; display:inline;">${JSON.stringify(config, undefined, 2)}</pre></details>`;

  debugText += '<hr style="border-color:#666"/>';

  debugText += '<details><summary>Controller:Action</summary><pre style="color:#777; display:inline;">'
    + `${Controller.name} : ${request.params.action}</pre></details>`;

  debugText += '<hr style="border-color:#666"/>';

  debugText += `<details><summary>Session</summary><pre style="color:#777; display:inline;">${JSON.stringify(request.session, undefined, 2)}</pre></details>`;

  debugText += '<hr style="border-color:#666"/>';

  result.body += '<div id="kohanajs-debug-panel" style="background-color: #000; color: #AAA; font-family: monospace; '
    + `font-size: 12px; padding: 1em; position: relative; z-index: 9999;">${debugText}</div>`;

  return result;
};

module.exports = {
  execute,
};
