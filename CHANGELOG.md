# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### accept tags: Added, Changed, Removed, Deprecated, Removed, Fixed, Security

## [2.2.2] - 2021-09-08
### Fixed
- express cannot output request.raw.url

## [2.2.1] - 2021-09-07
### Fixed
- did not output benchmark for json response

## [2.2.0] - 2021-09-05
### Added
- output benchmark to console
- create CHANGELOG.md

[Unreleased]: https://github.com/kohana-js/route-debug/-/compare